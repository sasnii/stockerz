<?php

require_once "Repository.php";
require_once __DIR__.'//..//Models//Currency.php';
require_once __DIR__.'//..//Models//Wallet.php';

class ShoppingRepository extends Repository {

    public function getValue(string $name): ?Currency 
    {
        $stmt = $this->database->connect()->prepare('
            SELECT * FROM currency WHERE name = :nem
        ');
        $stmt->bindParam(':name', $name, PDO::PARAM_STR);
        $stmt->execute();

        $user = $stmt->fetch(PDO::FETCH_ASSOC);

        if($user == false) {
            return null;
        }

        return new Currency(
            $user['name'],
            $user['value'],
        );
    }

    public function getCurrencies(): array {
        $result = [];
        $stmt = $this->database->connect()->prepare('
            SELECT * FROM currency
        ');
        $stmt->execute();
        $currency = $stmt->fetchAll(PDO::FETCH_ASSOC);

        foreach ($currency as $currency) {
            $result[] = new Currency(
                $currency['name'],
                $currency['value']
            );
        }
        return $result;
    }

    public function getWalletById(int $id): ?Wallet 
    {
        $stmt = $this->database->connect()->prepare('
        SELECT users.id, uwallet.PLNamount, uwallet.USDamount, uwallet.EURamount
        FROM users INNER JOIN uwallet
        ON users.id = uwallet.id WHERE users.id = :id
        ');
        $stmt->bindParam(':id', $id, PDO::PARAM_INT);
        $stmt->execute();

        $wallet = $stmt->fetch(PDO::FETCH_ASSOC);

        if($wallet == false) {
            return null;
        }

        return new Wallet(
            $wallet['id'],
            $wallet['PLNamount'],
            $wallet['USDamount'],
            $wallet['EURamount']
        );
    }

    public function updateWalletUSD(string $USDamount, int $id)
    {
        $pdo = $this->database->connect();
        try {
            $pdo->beginTransaction();
            $stmt = $pdo->prepare('
            UPDATE uwallet SET USDamount = (USDamount + :USD) WHERE id = :id
            ');
            $stmt->bindParam(':USD', $USDamount, PDO::PARAM_STR);
            $stmt->bindParam(':id', $id, PDO::PARAM_INT);
            $stmt->execute();

            $pdo->commit();
        } catch(\Exception $e) {
            $pdo->rollback();
            throw $e;
        }
    }

    public function updateWalletEUR(string $EURamount, int $id)
    {
        $pdo = $this->database->connect();
        try {
            $pdo->beginTransaction();
            $stmt = $pdo->prepare('
                UPDATE uwallet SET EURamount = (EURamount + :EUR) WHERE id = :id
            ');
            $stmt->bindParam(':EUR', $EURamount, PDO::PARAM_STR);
            $stmt->bindParam(':id', $id, PDO::PARAM_INT);
            $stmt->execute();

            $pdo->commit();
        } catch(\Exception $e) {
            $pdo->rollback();
            throw $e;
        }
    }

    public function updateWalletPLN(string $PLNamount, int $id)
    {
        $pdo = $this->database->connect();
        try {
            $pdo->beginTransaction();
            $stmt = $pdo->prepare('
                UPDATE uwallet SET PLNamount = (PLNamount + :PLN) WHERE id = :id
            ');
            $stmt->bindParam(':PLN', $PLNamount, PDO::PARAM_STR);
            $stmt->bindParam(':id', $id, PDO::PARAM_INT);
            $stmt->execute();

            $pdo->commit();
        } catch(\Exception $e) {
            $pdo->rollback();
            throw $e;
        }
    }

    public function sellWalletUSD(string $USDamount, int $id)
    {
        $pdo = $this->database->connect();
        try {
            $pdo->beginTransaction();
            $stmt = $pdo->prepare('
            UPDATE uwallet SET USDamount = (USDamount - :USD), PLNamount = (PLNamount + (:USD*3.82)) WHERE id = :id
            ');
            $stmt->bindParam(':USD', $USDamount, PDO::PARAM_STR);
            $stmt->bindParam(':id', $id, PDO::PARAM_INT);
            $stmt->execute();

            $pdo->commit();
        } catch(\Exception $e) {
            $pdo->rollback();
            throw $e;
        }
    }

    public function sellWalletEUR(string $EURamount, int $id)
    {
        $pdo = $this->database->connect();
        try {
            $pdo->beginTransaction();
            $stmt = $pdo->prepare('
            UPDATE uwallet SET EURamount = (EURamount - :EUR), PLNamount = (PLNamount + (:EUR*4.26)) WHERE id = :id
            ');
            $stmt->bindParam(':EUR', $EURamount, PDO::PARAM_STR);
            $stmt->bindParam(':id', $id, PDO::PARAM_INT);
            $stmt->execute();

            $pdo->commit();
        } catch(\Exception $e) {
            $pdo->rollback();
            throw $e;
        }
    }



    public function buyWalletUSD(string $USDamount, int $id)
    {
        $pdo = $this->database->connect();
        try {
            $pdo->beginTransaction();
            $stmt = $pdo->prepare('
            UPDATE uwallet SET USDamount = (USDamount + :USD), PLNamount = (PLNamount - (:USD*3.82)) WHERE id = :id
            ');
            $stmt->bindParam(':USD', $USDamount, PDO::PARAM_STR);
            $stmt->bindParam(':id', $id, PDO::PARAM_INT);
            $stmt->execute();

            $pdo->commit();
        } catch(\Exception $e) {
            $pdo->rollback();
            throw $e;
        }
    }

    public function buyWalletEUR(string $EURamount, int $id)
    {
        $pdo = $this->database->connect();
        try {
            $pdo->beginTransaction();
            $stmt = $pdo->prepare('
            UPDATE uwallet SET EURamount = (EURamount + :EUR), PLNamount = (PLNamount - (:EUR*4.26)) WHERE id = :id
            ');
            $stmt->bindParam(':EUR', $EURamount, PDO::PARAM_STR);
            $stmt->bindParam(':id', $id, PDO::PARAM_INT);
            $stmt->execute();

            $pdo->commit();
        } catch(\Exception $e) {
            $pdo->rollback();
            throw $e;
        }
    }
}
