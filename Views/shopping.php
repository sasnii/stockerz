<?php
if (!isset($_SESSION['id']) and !isset($_SESSION['role'])) {
  die('You are not logged in!');
}

if (!in_array('ROLE_USER', $_SESSION['role'])) {
  die('You do not have permission to watch this page!');
}
?>

<!DOCTYPE html>

<head>
  <meta charset="UTF-8">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.6/umd/popper.min.js"></script>
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
  <link rel="Stylesheet" type="text/css" href="../Public/css/style.css" />
  <link rel="Stylesheet" type="text/css" href="../Public/css/wallet.css" />
  <style>
    .input-group {
      background: red;
    }

    input[type=text],
    select,
    textarea {
      width: 100%;
      padding: 12px;
      border: 1px solid #ccc;
      border-radius: 4px;
      box-sizing: border-box;
      margin-top: 6px;
      margin-bottom: 16px;
      resize: vertical;
    }

    input[type=submit] {
      background-color: #E040FB;
      color: white;
      padding: 12px 20px;
      border: none;
      border-radius: 4px;
      cursor: pointer;
    }

    input[type=submit]:hover {
      background-color: #aaa;
    }
  </style>
  <link href="https://fonts.googleapis.com/css?family=Ubuntu&display=swap" rel="stylesheet">
  <script src="https://kit.fontawesome.com/723297a893.js" crossorigin="anonymous"></script>
  <title>Stockerz - shopping</title>
</head>

<body>
  <header>
    <?php include(dirname(__DIR__) . '/Views/navbar.php'); ?>
  </header>
  <div class="wrapit">
    <div class="column3">
      <div class="box">
        <form action="?page=updateWallet" method="POST">
          <label for="amount">Kwota</label>
          <input type="text" id="amount" name="amount" placeholder="100,00">
          <label for="currency">Waluta</label>
          <select id="currency" name="currency">
            <option value="PLN">PLN</option>
            <option value="EUR">EUR</option>
            <option value="USD">USD</option>
          </select>
          <label for="subject">Notatka</label>
          <textarea id="subject" name="subject" placeholder="Napisz notatkę (prowizja %)" style="height:200px"></textarea>
          <input type="submit" style="background-color: #4FC3F7;" value="Wpłać">
        </form>
      </div>
    </div>

    <div class="column3">
      <div class="box">
        <form action="?page=buyWallet" method="POST">
          <label for="amount">Kwota</label>
          <input type="text" id="amount" name="amount" placeholder="100,00">
          <label for="currency">Waluta</label>
          <select id="currency" name="currency">
            <option value="EUR">EUR</option>
            <option value="USD">USD</option>
          </select>
          <input type="submit" style="background-color: #45a049;" value="Kup">
        </form>
      </div>
    </div>

    <div class="column3">
      <div class="box">
        <form action="?page=sellWallet" method="POST">
          <label for="amount">Kwota</label>
          <input type="text" id="amount" name="amount" placeholder="100,00">
          <label for="currency">Waluta</label>
          <select id="currency" name="currency">
            <option value="EUR">EUR</option>
            <option value="USD">USD</option>
          </select>
          <input type="submit" style="background-color: #E040FB;" value="Sprzedaj">
        </form>
      </div>
    </div>


  </div>


</body>

</html>