<?php
if (!isset($_SESSION['id']) and !isset($_SESSION['role'])) {
    die('You are not logged in!');
}

if (!in_array('ROLE_USER', $_SESSION['role'])) {
    die('You do not have permission to watch this page!');
}
?>

<!DOCTYPE html>

<head>
    <meta charset="UTF-8">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.6/umd/popper.min.js"></script>
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">
        google.charts.load("current", {
            packages: ["corechart"]
        });
        google.charts.setOnLoadCallback(drawChart);

        function drawChart() {
            var data = google.visualization.arrayToDataTable([
                ['Waluta', 'Wartość w PLN'],
                ['PLN', <?= $uwallet->getPLNamount(); ?>],
                ['USD', <?= $uwallet->getUSDamount() * 3.82 ?>],
                ['EUR', <?= $uwallet->getEURamount() * 4.26 ?>],
            ]);

            var options = {
                pieHole: 0.6,
                legend: {
                    position: 'bottom'
                },
            };
            var chart = new google.visualization.PieChart(document.getElementById('donutchart'));
            chart.draw(data, options);
        }
    </script>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="Stylesheet" type="text/css" href="../Public/css/style.css" />
    <link rel="Stylesheet" type="text/css" href="../Public/css/wallet.css" />
    <link href="https://fonts.googleapis.com/css?family=Ubuntu&display=swap" rel="stylesheet">
    <script src="https://kit.fontawesome.com/723297a893.js" crossorigin="anonymous"></script>
    <title>Stockerz - wallet</title>
</head>

<body>
    <header>
        <?php include(dirname(__DIR__) . '/Views/navbar.php'); ?>
    </header>
    <div class="wrapit">
        <div class="column3">
            <div class="sum box">
                <h2>Wartość portfela:</h2>
                <h4><?= $total = $uwallet->getPLNamount() + ($uwallet->getEURamount() * 4.26) + ($uwallet->getUSDamount() * 3.82); ?> PLN
                </h4>
            </div>
            <div class="summary box">
            <h2>Ceny walut</h2>
                <table class="table mt-4 text-dark">
                    <thead>
                        <tr>
                            <th scope="col">Waluta</th>
                            <th scope="col">Średnia cena zakupu</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($currencies as $currencies) : ?>
                            <tr>
                                <th scope="row"><?= $currencies->getName(); ?></th>
                                <td><?= $currencies->getValue(); ?></td>
                            </tr>
                        <?php endforeach ?>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="column3">
            <div class="box">
                <div style="height:100%; background-color: #FFFFFF; overflow:hidden; box-sizing: border-box;  border-radius: 4px; text-align: right; line-height:14px; font-size: 12px; font-feature-settings: normal; text-size-adjust: 100%; box-shadow: inset 0 -20px 0 0 #56667F;padding:1px;padding: 0px; margin: 0px; width: 100%;">
                    <div style="height:540px; padding:0px; margin:0px; width: 100%;"><iframe src="https://widget.coinlib.io/widget?type=chart&theme=light&coin_id=859&pref_coin_id=1505" width="100%" height="536px" scrolling="auto" marginwidth="0" marginheight="0" frameborder="0" style="border:0;margin:0;padding:0;line-height:14px;"></iframe></div>
                    <div style="color: #FFFFFF; line-height: 14px; font-weight: 400; font-size: 11px; box-sizing: border-box; padding: 2px 6px; width: 100%; font-family: Verdana, Tahoma, Arial, sans-serif;"><a href="https://coinlib.io/widgets" target="_blank" style="font-weight: 500; color: #FFFFFF; text-decoration:none; font-size:11px">Bitcoin Widget</a>&nbsp;by Coinlib</div>
                </div>
            </div>
        </div>
        <div class="column3">
            <div class="box">
                <table class="table mt-4 text-dark">
                <h2>Twój portfel:</h2>
                    <thead>
                        <tr>
                            <th scope="col">PLN</th>
                            <th scope="col">USD</th>
                            <th scope="col">EUR</th>
                            <th scope="col">Podsumowanie (PLN)</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td><?= $uwallet->getPLNamount(); ?></td>
                            <td><?= $uwallet->getUSDamount(); ?></td>
                            <td><?= $uwallet->getEURamount(); ?></td>
                            <td><?=
                                    $total = $uwallet->getPLNamount() + ($uwallet->getEURamount() * 4.26) + ($uwallet->getUSDamount() * 3.82);
                                ?></td>
                        </tr>
                    </tbody>
                </table>
                <div id="donutchart"></div>
            </div>
        </div>
    </div>
</body>

</html>