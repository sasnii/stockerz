<?php
if (!isset($_SESSION['id']) and !isset($_SESSION['role'])) {
  die('You are not logged in!');
}

if (!in_array('ROLE_USER', $_SESSION['role'])) {
  die('You do not have permission to watch this page!');
}
?>

<!DOCTYPE html>

<head>
  <meta charset="UTF-8">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.6/umd/popper.min.js"></script>
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
  <link rel="Stylesheet" type="text/css" href="../Public/css/style.css" />
  <link rel="Stylesheet" type="text/css" href="../Public/css/wallet.css" />
  <link href="https://fonts.googleapis.com/css?family=Ubuntu&display=swap" rel="stylesheet">
  <script src="https://kit.fontawesome.com/723297a893.js" crossorigin="anonymous"></script>
  <title>Stockerz - wallet</title>
</head>

<body>
  <header>
    <?php include(dirname(__DIR__) . '/Views/navbar.php'); ?>
  </header>
  <div class="column1">
    <div class="box">

    </div>
  </div>
</body>

</html>