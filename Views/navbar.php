<div class="navbarek">
    <a href="?page=wallet"><img src="../Public/img/logo.svg"></a>
    <a href="?page=analize"><i class="fas fa-chart-line"></i>  Analiza</a>
    <a href="?page=wallet"><i class="fas fa-wallet"></i>  Portfel</a>
    <a href="?page=shopping"><i class="fas fa-random"></i></i>  Zakupy</a>
    <?php
    
    if(!isset($_SESSION['id']))
    {
        $url = "http://$_SERVER[HTTP_HOST]/";
        header("Location: {$url}?page=error");
        return;
    }
    
    require_once __DIR__.'/../Repository/UserRepository.php';

    $userRepository = new UserRepository();
    if($userRepository->getUserByEmail($_SESSION['id'])->getId() === 1)
    {
        echo "<a href=\"?page=users\">
                <i class=\"fas fa-user-shield\"></i>
            </a>";
    }
    
    ?>
    
    <a href="?page=logout" style="float: right;"><img src="../Public/img/uploads/profile.jpg" class="profile_nav">  <?= $_SESSION['id'] ?></a>

</div>