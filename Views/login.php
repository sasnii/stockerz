<!DOCTYPE html>
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="Stylesheet" type="text/css" href="../Public/css/style.css" />
    <link href="https://fonts.googleapis.com/css?family=Ubuntu&display=swap" rel="stylesheet">
    <script src="https://kit.fontawesome.com/723297a893.js" crossorigin="anonymous"></script>
    <script src=""></script>
    <title>Stockerz - best app ever</title>
</head>
<body>
<div class="container">
    <div class="logo">
        <img src="../Public/img/uploads/logo_s.png">
    </div>
    <div class="rightLogin">
    <div class="loginORregister">
        <a href="?page=login"><button type="submit">LOGOWANIE</button></a>
        <a href="?page=register"><button type="submit">REJETRACJA</button></a>
    </div>
    <form action="?page=login" method="POST">
        <div class="messages">
            <?php
                if(isset($messages)){
                    foreach($messages as $message) {
                        echo $message;
                    }
                }
            ?>
        </div>
        <input name="email" type="text" placeholder="Email">
        <input name="password" type="password" placeholder="Hasło">
        <button type="submit">DALEJ</button>
    </form>
    <div class="forgotPassword">
        <a href="#"><p>Zapomniałeś hasła?</p></a>
    </div>
    </div>
</div>
</body>
</html>