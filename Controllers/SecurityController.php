<?php

require_once 'AppController.php';
require_once __DIR__.'//..//Models//User.php';
require_once __DIR__.'//..//Repository//UserRepository.php';

class SecurityController extends AppController {

    public function login()
    {   
        if(isset($_SESSION['id']))
        {
            $url = "http://$_SERVER[HTTP_HOST]/";
            header("Location: {$url}?page=wallet");
            return;
        }
        $userRepository = new UserRepository();

        if ($this->isPost()) {
            $email = $_POST['email'];
            $password = $_POST['password'];

            $user = $userRepository->getUser($email);

            if (!$user) {
                $this->render('login', ['messages' => ['Użytkownik z tym mailem nie istnieje.']]);
                return;
            }

            if ($user->getPassword() !== $password) {
                $this->render('login', ['messages' => ['Złe hasło!']]);
                return;
            }

            $_SESSION["id"] = $user->getEmail();
            $_SESSION["role"] = $user->getRole();

            $url = "http://$_SERVER[HTTP_HOST]/";
            header("Location: {$url}?page=wallet");
            return;
        }
        $this->render('login');
    }

    public function register()
    {
        if(isset($_SESSION['id']))
        {
            $url = "http://$_SERVER[HTTP_HOST]/";
            header("Location: {$url}?page=wallet");
            return;
        }
        $userRepository = new UserRepository();

        if ($this->isPost()) {
            $email = $_POST['email'];
            $name = $_POST['name'];
            $password = $_POST['password'];
            $repeated_password = $_POST['repeated_password'];

            if(strlen($password) < 5) {
                $this->render('register', ['messages' => ['Hasło zbyt słabe']]);
                return;
            }

            if($password !== $repeated_password) {
                $this->render('register', ['messages' => ['Hasła do siebie nie pasują']]);
                return;
            }

            $user = $userRepository->getUserByEmail($email);

            if ($user) {
                $this->render('register', ['messages' => ['Użytkownik z tym emailem już istnieje']]);
                return;
            }

            $user = $userRepository->getUserByName($name);

            if ($user) {
                $this->render('register', ['messages' => ['Użytkownik z tym imieniem już istnieje']]);
                return;
            }

            $userRepository->addUser($email, $name, $password);

            $this->render('login', ['messages' => ['Teraz możesz się zalogować.']]);
            return;
        }
        $this->render('register');
    }

    public function logout()
    {
        session_unset();
        session_destroy();

        $this->render('login', ['messages' => ['Wylogowano!']]);
    }
}