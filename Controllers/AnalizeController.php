<?php

require_once 'AppController.php';
require_once __DIR__.'//..//Database.php';

class AnalizeController extends AppController {

    public function analize()
    {   
        $database = new Database();
        $database->connect();
        $this->render('analize');
    }
}