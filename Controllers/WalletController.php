<?php

require_once 'AppController.php';
require_once __DIR__.'//..//Models//Currency.php';
require_once __DIR__.'//..//Repository//ShoppingRepository.php';

class WalletController extends AppController {

    public function wallet()
    {   
        $database = new Database();
        $database->connect();

        $ShoppingRepository = new ShoppingRepository();
        $currencies = $ShoppingRepository->getCurrencies();
        $userRepository = new UserRepository();
        $uwallet = $ShoppingRepository->getWalletById($userRepository->getUserByEmail($_SESSION['id'])->getId());

        $this->render2('wallet', ['uwallet' => $uwallet], ['currencies' => $currencies]);
    }
}