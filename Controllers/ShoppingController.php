<?php

require_once 'AppController.php';
require_once __DIR__ . '//..//Database.php';
require_once __DIR__ . '//..//Repository//ShoppingRepository.php';

class ShoppingController extends AppController
{
    public function shopping()
    {
        $database = new Database();
        $database->connect();
        $this->render('shopping', ['posts']);
    }

    public function updateWallet()
    {
        $shoppingRepository = new ShoppingRepository();
        $userRepository = new UserRepository();
        if ($this->isPost()) {
            $amount = $_POST['amount'];
            $currency = $_POST['currency'];

            if ($currency == 'PLN')
            $shoppingRepository->updateWalletPLN($amount, $userRepository->getUserByEmail($_SESSION['id'])->getId());

            if ($currency == 'USD')
                $shoppingRepository->updateWalletUSD($amount, $userRepository->getUserByEmail($_SESSION['id'])->getId());

            if ($currency == 'EUR')
                $shoppingRepository->updateWalletEUR($amount, $userRepository->getUserByEmail($_SESSION['id'])->getId());

            $this->render('shopping');
            return;
        }
        $this->render('wallet');
    }



    public function sellWallet()
    {
        $shoppingRepository = new ShoppingRepository();
        $userRepository = new UserRepository();
        if ($this->isPost()) {
            $amount = $_POST['amount'];
            $currency = $_POST['currency'];

            if ($currency == 'USD')
                $shoppingRepository->sellWalletUSD($amount, $userRepository->getUserByEmail($_SESSION['id'])->getId());

            if ($currency == 'EUR')
                $shoppingRepository->sellWalletEUR($amount, $userRepository->getUserByEmail($_SESSION['id'])->getId());

            $this->render('shopping');
            return;
        }
        $this->render('wallet');
    }

    public function buyWallet()
    {
        $shoppingRepository = new ShoppingRepository();
        $userRepository = new UserRepository();
        if ($this->isPost()) {
            $amount = $_POST['amount'];
            $currency = $_POST['currency'];

            if ($currency == 'USD')
                $shoppingRepository->buyWalletUSD($amount, $userRepository->getUserByEmail($_SESSION['id'])->getId());

            if ($currency == 'EUR')
                $shoppingRepository->buyWalletEUR($amount, $userRepository->getUserByEmail($_SESSION['id'])->getId());

            $this->render('shopping');
            return;
        }
        $this->render('wallet');
    }
}
