<?php

class Wallet {
    private $id;
    private $PLNamount;
    private $USDamount;
    private $EURamount;

    public function __construct(
        int $id,
        float $PLNamount,
        float $USDamount,
        float $EURamount
    
    ) {
        $this->id = $id;
        $this->PLNamount = $PLNamount;
        $this->USDamount = $USDamount;
        $this->EURamount = $EURamount;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getPLNamount(): float
    {
        return $this->PLNamount;
    }

    public function getUSDamount(): float
    {
        return $this->USDamount;
    }

    public function getEURamount(): float
    {
        return $this->EURamount;
    }

}