<?php

require_once 'Controllers//SecurityController.php';
require_once 'Controllers//AdminController.php';
require_once 'Controllers//WalletController.php';
require_once 'Controllers//ShoppingController.php';
require_once 'Controllers//AnalizeController.php';

class Routing {
    private $routes = [];

    public function __construct()
    {
        $this->routes = [
            'login' => [
                'controller' => 'SecurityController',
                'action' => 'login'
            ],
            'logout' => [
                'controller' => 'SecurityController',
                'action' => 'logout'
            ],
            'users' => [
                'controller' => 'AdminController',
                'action' => 'users'
            ],
            'wallet' => [
                'controller' => 'WalletController',
                'action' => 'wallet'
            ],
            'shopping' => [
                'controller' => 'ShoppingController',
                'action' => 'shopping'
            ],
            'analize' => [
                'controller' => 'AnalizeController',
                'action' => 'analize'
            ],
            'register' => [
                'controller' => 'SecurityController',
                'action' => 'register'
            ],
            'updateWallet' => [
                'controller' => 'ShoppingController',
                'action' => 'updateWallet'
            ],
            'buyWallet' => [
                'controller' => 'ShoppingController',
                'action' => 'buyWallet'
            ],
            'sellWallet' => [
                'controller' => 'ShoppingController',
                'action' => 'sellWallet'
            ]
        ];
    }

    public function run()
    {
        $page = isset($_GET['page']) ? $_GET['page'] : 'login';

        if (isset($this->routes[$page])) {
            $controller = $this->routes[$page]['controller'];
            $action = $this->routes[$page]['action'];

            $object = new $controller;
            $object->$action();
        }
    }
}